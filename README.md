#  Archive Job Data

## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Future Enhancements](#future-enhancements)
* [Attributes](#attributes)
* [Additional Information](#additional-information)

## Overview

This pre-built allows IAP users to archive the `jobs` and `tasks` collections to keep IAP performant. Users have the ability to choose:
1. The number of days older than which, all qualifying data will be archived.
2. Custom names for the archive collections to store the matching `jobs` and `tasks` documents.
3. To keep or delete the matching documents from the original `jobs` and `tasks` collections after backing them up.

<table><tr><td>
<img  src="./images/workflow.png"  alt="workflow"  width="1000px">
</td></tr></table>

Users can use the included Automation Catalog item to schedule the pre-built to run at regular intervals.

Notes: 
1. For every run, new jobs and tasks collections would be created with the current timestamps appended to the end of the jobs and tasks collection names put in by the user.
2. Only jobs and tasks related to jobs that are either in *complete* or *canceled* state will be considered for archiving. Jobs that are in errored or incomplete state will be left untouched.
3. Getting an `RangeError [ERR_OUT_OF_RANGE]` typically indicates that your bundle size is too large. Try setting it lower within the automation catalog. 
4. Very large databases with a lot of jobs and tasks to archive will typically result in multiple archive collections within the database. 

## Installation Prerequisites

Users must satisfy the following prerequisites:

* Itential Automation Platform: `^2022.1`
* JST: `^3.9.x`
* [Adapter DB Mongo](https://gitlab.com/itentialopensource/adapters/persistence/adapter-db_mongo/): `^0.4.5`
  
## How to Install

To install the pre-built:
* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section.
* The pre-built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-built and click the install button (as shown below).
  
<table><tr><td>
<img  src="./images/install.gif"  alt="install"  width="1000px">
</td></tr></table>

## How to Run

Use the following steps to run the pre-built:
1. Once the pre-built is installed as outlined in the [How to Install](#how-to-install) section above, go to `Automation Catalog` and run the `Archive Jobs and Tasks Collections` automation. 
2. In the `Copy/Move Jobs data to Collection` and the `Copy/Move Tasks data to Collection` fields, enter the names for collections you want the matching `jobs` and `tasks` collection documents to be copied to.
3. In the `Copy/Move data from before # days` field, enter the number of days (from the current date) oolder than which you want data to be archived. *Note: Please enter a positive number*
4. In the `MongoDB Adapter` dropdown menu, select the adapter that is connected the MongoDB that contains the Tasks and Jobs Collections that you want to create an archive of.
5. In the `Batch Size` field, select how many jobs you want to archive at a time. 
6. Check the `Create Archive Collections` checkbox if you want to create an archive collection that will be stored in the MongoDB. Unchecking this box will mean no archive will be created. 
7. Check the `Delete copied data from collections` checkbox if you want to discard the matching documents from the original `jobs` and `tasks` collections after they have been backed up. 
8. Check the `Verbose` checkbox if you want to see the results of the pre-built at the end of its run and click on `START` and then on `VIEW JOB`.

Note: The adapter ID for mongoDB in all the respective tasks of the workflow must be changed accordingly.

Here is a sample run down of the steps:
<table><tr><td>
<img  src="./images/ac-item.gif"  alt="ac-item"  width="1000px">
</td></tr></table>

## Future Enhancement(s)

1. Support for archiving the data into one collection each for jobs and tasks (rather than creating new collections for each run)

## Attributes

The input attributes for the pre-built are outlined in the following table. To run the workflow directly from IAP/API call (and not through Automation Catalog), please encapsulate all of these attributes into a root object with the key `formData`.

<table  border='1'  style='border-collapse:collapse'>
<thead>
<tr>
<th>Attribute</th>
<th>Description</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>copyMoveJobsDataToCollection</code></td>
<td>Enter the name of the desired collection (new or pre-existing) where you want the matching documents from the `jobs` collection to be archived.</td>
<td><code>string</code></td>
</tr>
<tr>
<td><code>copyMoveTasksDataToCollection</code></td>
<td>Enter the name of the desired collection (new or pre-existing) where you want the matching documents from the `tasks` collection to be archived.</td>
<td><code>string</code></td>
</tr>
<tr>
<tr>
<td><code>copyMoveDataFromBeforeDays</code></td>
<td>Enter the number of days (from the current date) older than which all data will be archived.</td>
<td><code>integer</code></td>
</tr>
<tr>
<td><code>deleteCopiedDataFromCollections</code></td>
<td>Send as <code>true</code>/<code>false</code> to discard/keep the matching documents from the original `jobs` and `tasks` collections after they have been backed up.</td>
<td><code>boolean</code></td>
</tr>
<tr>
<td><code>verbose</code></td>
<td>Send as <code>true</code>/<code>false</code> to not see/see the results of the pre-built at the end of its run.</td>
<td><code>boolean</code></td>
</tr>
</tbody>
</table>
  
### Sample Input:

```
{
    "formData": {
    "createArchiveCollections": true,
    "deleteCopiedDataFromCollections": true,
    "verbose": true,
    "createArchiveCollection": true,
    "copyMoveJobsDataToCollection": "jobs_archive",
    "copyMoveTasksDataToCollection": "tasks_archive",
    "copyMoveDataOlderThanDays": 3,
    "batchSize": 2000,
    "mongoDbAdapter": "mongoPronghorn"
  }
}
```

### Sample Output:
(only for when `verbose` is set as `true`)
<table><tr><td>
<img  src="./images/output.png"  alt="output"  width="600px">
</td></tr></table>

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
