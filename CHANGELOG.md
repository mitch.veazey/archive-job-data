
## 0.0.13 [07-22-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/archive-job-data!17

---

## 0.0.12 [02-28-2022]

* Fix JSON Form batch min/max values and README

See merge request itentialopensource/pre-built-automations/archive-job-data!14

---

## 0.0.11 [07-23-2021]

* Update package.json, README.md, bundles/ac_agenda_jobs/Archive Jobs and Tasks...

See merge request itentialopensource/pre-built-automations/archive-job-data!11

---

## 0.0.10 [07-23-2021]

* Update pre-built to overcome the 16mb limit

See merge request itentialopensource/pre-built-automations/archive-job-data!9

---

## 0.0.9 [07-13-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/archive-job-data!7

---

## 0.0.8 [07-13-2021]

* Update bundles/ac_agenda_jobs/Archive Jobs and Tasks Collections.json,...

See merge request itentialopensource/pre-built-automations/archive-job-data!5

---

## 0.0.7 [07-06-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/archive-job-data!4

---

## 0.0.6 [03-18-2021]

* patch/LB-515

See merge request itentialopensource/pre-built-automations/archive-job-data!3

---

## 0.0.5 [10-15-2020]

* View data will show number of jobs affected, updated transformations, workflows, improved verbage

See merge request itentialopensource/pre-built-automations/archive-job-data!2

---

## 0.0.4 [10-09-2020]

* Updated README.md

See merge request itentialopensource/pre-built-automations/staging/archive-job-data!1

---

## 0.0.3 [10-08-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.2 [10-08-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---
